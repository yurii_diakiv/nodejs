const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const result = UserRepository.getOne(search);
        //s
        if (!result) {
            throw new Error('User not found');
        }
        //e  
        return result;
    }

    //s
    create(user) {
        return UserRepository.create(user);
    }

    getAll(){
        return UserRepository.getAll();
    }

    update(id, data){
        const result = UserRepository.update(id, data);
        if (!result.id) {
            throw new Error('User not found(wrong id)');
        }
        return result;
    }
    
    delete(id){
        const result = UserRepository.delete(id);
        if (!result.length) {
            throw new Error('User not found');
        }
        return result;
    }
    //e
}

module.exports = new UserService();