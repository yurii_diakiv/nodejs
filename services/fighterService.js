const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    //s
    create(fighter) {
        return FighterRepository.create(fighter);
    }

    getAll(){
        return FighterRepository.getAll();
    }

    getOne(search){
        const result = FighterRepository.getOne(search);
        if (!result) {
            throw new Error('Fighter not found');
        }
        return result;
    }

    update(id, data){
        const result = FighterRepository.update(id, data);
        if (!result.id) {
            throw new Error('Fighter not found(wrong id)');
        }
        return result;
    }
    
    delete(id){
        const result = FighterRepository.delete(id);
        if (!result.length) {
            throw new Error('Fighter not found');
        }
        return result;
    }
    //e
}

module.exports = new FighterService();