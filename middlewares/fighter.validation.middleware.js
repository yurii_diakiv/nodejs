const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    //s
    validateFighter(req, res);
    //e
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    //s
    validateFighter(req, res);
    //e
    next();
}

//s
function validateFighter(req, res) {
    let { id, health, ...testFighter } = fighter;
    let body = req.body;
    let err;

    if (!Object.keys(testFighter).every((x) => x in body)) {
        err = 'There are no all necessary properties';
    }

    else if ('id' in body) {
        err = "There is id";
    }

    else if (!(typeof body.power === 'number' && body.power > 0 && body.power < 100)) {
        err = 'Not valid power';
    }

    else if (!(typeof body.defense === 'number' && body.defense >= 1 && body.defense <= 10)) {
        err = 'Not valid defense';
    }

    else if (Object.keys(body).length != 3) {
        err = 'There are extra fields';
    }

    if (err) {
        res.locals.validationErr = err;
    }
}
//e

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;