const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    //s
    validateUser(req, res);
    //e
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    //s
    validateUser(req, res);
    //e
    next();
}

//s
function validateUser(req, res) {
    let { id, ...testUser } = user;
    let body = req.body;
    let err;
    if (!Object.keys(testUser).every((x) => x in body)) {
        err = 'There are no all necessary properties';
    }

    else if ('id' in body) {
        err = "There is id";
    }

    else if (!/@gmail\.com$/.test(body.email)) {
        err = 'Not valid email';
    }

    else if (!(/\+380\d{9}/.test(body.phoneNumber) && body.phoneNumber.length === 13)) {
        err = 'Not valid phone number';
    }

    else if (body.password.length < 3) {
        err = 'Not valid password';
    }

    else if (Object.keys(body).length != 5) {
        err = 'There are extra fields';
    }

    if (err) {
        res.locals.validationErr = err;
    }
}
//e

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;