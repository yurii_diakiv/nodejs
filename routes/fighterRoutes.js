const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
//s
router.get('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = FighterService.getOne({ id });
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('', (req, res, next) => {
    try {
        const data = FighterService.getAll();
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('', createFighterValid, (req, res, next) => {
    try {
        if (!res.locals.validationErr) {
            const data = FighterService.create(req.body);
            res.locals.data = data;
        }
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        if (!res.locals.validationErr) {
            let id = req.params.id;
            const data = FighterService.update(id, req.body);
            res.locals.data = data;
        }
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = FighterService.delete(id);
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);
//e

module.exports = router;